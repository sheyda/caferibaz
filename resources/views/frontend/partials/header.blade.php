<header>
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide" src="{{asset('/img/1slide_.jpg')}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="first-slide" src="{{asset('/img/2slide_.jpg')}}" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="first-slide" src="{{asset('/img/3slide_.jpg')}}" alt="First slide">
                <div class="container">
                    <div class="carousel-caption text-right">
                        <h1>کافه ریباز</h1>
                        <p>انواع متن تستی پذیرفته میشود. </p>
                        <p><a class="btn btn-lg btn-primary" href="#" role="button">محبوب ترین محصولات</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</header>
