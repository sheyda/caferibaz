@extends('frontend.mainlayout')
@section('content')
    <div class="album py-5 bg-light">
        <div class="container">
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
                <h1 class="h5">{{$category_id->title}}</h1>
                <div class="btn-toolbar mb-2 mb-md-0">

                </div>
            </div>
            <div class="row">
                @foreach($productGroup as $product)
                    <div class="col-12 col-md-3">
                        <div class="card mb-4 box-shadow">
                            <img class="card-img-top"
                                 src="{{asset('storage/'.$product->icon)}}"
                                 alt="Card image cap" width="140" height="140">
                            <div class="card-body">
                                <h5 class="card-text">{{$product->title}}</h5>
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-sm btn-outline-secondary">خرید</button>
                                    </div>
                                    <small class="text-muted">{{$product->price .'تومان'}}</small>
                                </div>
                            </div>
                        </div>
                    </div>

                @endforeach
            </div>
        </div>
        @include('dashboard.pagation_default', ['paginator' => $productGroup])

    </div>

@endsection







