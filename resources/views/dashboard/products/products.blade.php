@extends('dashboard.master')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div style="margin: 20px">

        <a class="btn btn-primary"
           href="{{route('product.create')}}">{{__('validation.attributes.add-new-product')}}
        </a>

        <span>
            <form action="" style="display: inline-block;float: right ;margin-right:20px ">
        <label for="category_name">دسته بندی</label>
        <select name="category_id" id="category_name">
            <option value=""></option>
                       @foreach($category_products as $category_product)
                <option value="{{$category_product->id}}">"{{$category_product->title}}"</option>
            @endforeach
        </select>
        <input type="submit" value="جستجو">
    </form>

        </span>
    </div>
    <div class="container marketing">
        <div>
            <table class="table table-striped">
                <thead class="col">
                <tr>
                    <th>#</th>
                    <th>{{__('validation.attributes.title')}}</th>
                    <th>{{__('validation.attributes.category')}}</th>
                    <th>{{__('validation.attributes.icon')}}</th>
                    <th>{{__('validation.attributes.quantity')}}</th>
                    <th>{{__('validation.attributes.price')}}</th>
                    <th>{{__('validation.attributes.discount')}}</th>
                    <th>{{__('validation.attributes.status')}}</th>
                    <th>{{__('validation.attributes.operation')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $item)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$item->title}}</td>
                        <td>{{$item->getCategory->title}}</td>
                        <td>
                            <img id="preview" src="{{asset('storage/'.$item->icon)}}" style="width:50px;height:50px;">
                        </td>
                        <td>{{$item->quantity}}</td>
                        <td>{{$item->price}}</td>
                        <td>{{$item->discount}}</td>
                        <td>{{$item->status}}</td>
                        <td>{{$item->operations}}
                            <div class="btn-group">
                            <span><form method="GET" action="{{route('product.edit',['product_id'=>$item->id])}}">
                                    <button class="btn btn-link">ویرایش </button>
                                </form></span>
                                |
                                <span><form method="POST" action="{{ route('product.delete',$item->id) }}">
                        @csrf
                                        @method('delete')
                        <button class="btn btn-link" style="color: red">حذف</button>
                                </form>
                        </span>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @include('dashboard.pagation_default', ['paginator' => $products])
    </div>
@endsection


