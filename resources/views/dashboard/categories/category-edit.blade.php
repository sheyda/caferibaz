@extends('dashboard.master')

@section('content')
    <style>
        .box {
            width: 500px;
            height: auto;
            background-color: #e3e3e3;
            margin: 100px auto;
            border-radius: 10px;
        }

        .in-box {
            padding: 15px;
        }

        label{
            float: right;
        }
    </style>
    <div class="box">
        <div class="in-box form-group">
            <form action="{{url('admin/category/'.$category_id->id)}}" method="post" enctype="multipart/form-data">
                @method('PUT')
                <label for="discount">{{__('validation.attributes.title')}}</label>
                <input class="form-control" type="text" name="title" value="{{$category_id->title}}" >
                @error('title')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <div style="float: right">
                    <label for="files" class="btn-sm btn-info">{{__('validation.attributes.select-icon')}}</label>
                    <input data-preview="#preview" id="files" style="visibility:hidden;" type="file" name="icon" value="{{$category_id->icon}}">
                    @if($category_id->icon)
                        <img id="preview" src="{{asset('storage/'.$category_id->icon)}}" style="width:200px;height:200px;">
                    @endif

                </div>
                @error('icon')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <button type="submit" class="form-control btn btn-success">{{__('validation.attributes.edit')}}</button>
            </form>

{{--            <form action="{{route('category.delete', ['category_id'=>$category_id->id])}}" method="post">--}}
{{--                @method('delete')--}}
{{--                <button type="submit" class="btn btn-danger">{{__('validation.attributes.delete')}}</button>--}}
{{--            </form>--}}

        </div>
    </div>


@endsection


