@extends('dashboard.master')

@section('sidebar')
    @parent
@endsection

@section('content')
    <div class="container marketing">

    <div  style="margin: 15px">
        <a class="btn btn-primary" href="{{route('category.create')}}">{{__('validation.attributes.add-new-category')}}</a>
    </div>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>#</th>
            <th>{{__('validation.attributes.title')}}</th>
            <th>{{__('validation.attributes.icon')}}</th>
            <th>{{__('validation.attributes.operation')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($category as $key=>$item)
            <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$item->title}}</td>
                <td>
                    <img id="preview" src="{{asset('storage/'.$item->icon)}}" style="width:50px;height:50px;">
                </td>
                <td>{{$item->operations}}
                    <div class="btn-group">
                            <span><form method="GET" action="{{route('category.edit',['category_id'=>$item->id])}}">
                                    <button class="btn btn-link">ویرایش </button>
                                </form></span>
                        |
                        <span><form method="POST" action="{{ route('category.delete',$item->id) }}">
                        @csrf
                                @method('delete')
                        <button class="btn btn-link" style="color: red">حذف</button>
                                </form>
                        </span>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('dashboard.pagation_default', ['paginator' => $category])
    </div>
@endsection


