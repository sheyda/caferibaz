@extends('dashboard.master')

@section('content')
    <style>
        .box {
            width: 500px;
            height: auto;
            border-radius: 10px;
            background-color: #e3e3e3;
            margin: 100px auto;
        }

        .in-box {
            padding: 15px;
        }

        .form-error {

            border: 2px solid #e74c3c;
        }



    </style>
    <div class="box">
        <div class="in-box form-group">

            <form action="{{url('admin/category')}}" method="post" enctype="multipart/form-data">
                @method('POST')

                <label style="float: right" for="title">{{__('validation.attributes.title')}}</label>
                <input class="form-control" type="text" name="title" id="title" value="{{ old('title') }}">
                @error('title')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <div style="float:right;">
                    <label for="files" class="btn-sm btn-info">{{__('validation.attributes.select-icon')}}</label>
                    <input id="files" style="visibility:hidden;" type="file" name="icon">
                    <img id = "myid" src = "#" alt = "new image" />
                </div>
                @error('icon')
                <div style="color:#cc0000">
                    {{$message}}
                </div>
                @enderror
                <br>

                <button type="submit" class="form-control btn btn-success">{{__('validation.attributes.insert')}}</button>
            </form>
        </div>
    </div>

    <script>
        function display(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $('#myid').attr('src', event.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#file").change(function() {
            display(this);
        });
    </script>
@endsection
