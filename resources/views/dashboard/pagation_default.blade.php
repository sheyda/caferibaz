@if ($paginator->lastPage() > 1)
    <div class="center1">
    <div class="pagination1" >
        <span  class="{{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url(1) }}">قبلی</a>
        </span>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <span >
                <a class="{{ ($paginator->currentPage() == $i) ? ' active' : '' }}" href="{{ $paginator->url($i) }}">{{ $i }}</a>
            </span>
        @endfor
        <span class="{{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
            <a href="{{ $paginator->url($paginator->currentPage()+1) }}">بعدی</a>
        </span>
    </div>
    </div>
@endif
<style>
    .pagination1 a {

        display: inline-block;
        color: black;
        padding: 20px 20px 20px 20px;
        text-decoration: none;
    }

    .pagination1 a.active {
        background-color: #4CAF50;
        color: white;
        padding: 8px 16px;
        border-radius: 5px;
    }

    .pagination1 a:hover:not(.active) {
        background-color: #ddd;
        padding: 8px 16px;
        border-radius: 5px;
    }
    .center1 {
        text-align: center;
    }
</style>
