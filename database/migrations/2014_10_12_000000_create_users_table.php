<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users',
            function (Blueprint $table) {

                $table->charset = 'utf8';
                $table->collation = 'utf8_general_ci';

                $table->id();
                $table->string('first_name', 45);
                $table->string('last_name', 45);
                $table->string('user_name', 45);
                $table->string('password', 15);
                $table->string('email')->unique();
                $table->integer('phone_number')->comment('without first zero number.');
                $table->enum('level', ['user', 'admin']);
                $table->tinyInteger('status')
                      ->comment('status is 1 when a user account is active and it is 0 otherwise.');

                $table->rememberToken()->comment('save cookies');
                $table->timestamps();
                $table->softDeletes()->comment('deleted at');

            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('users');

    }
}
