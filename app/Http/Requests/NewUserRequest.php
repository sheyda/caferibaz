<?php

namespace App\Http\Requests;

use App\Rules\CheckArabicCharacters;
use Illuminate\Foundation\Http\FormRequest;

class NewUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'=>'required|string',
            'last_name'=>'required|string',
            'user_name'=>'required|unique:users|alpha_dash',
            'password'=>['required','alpha_dash',new CheckArabicCharacters()],
            'email'=>'required|unique:users|email',
            'phone_number'=>'required|unique:users|string|regex:/(09)[0-9]{9}/',
            'status'=>'required',
            'level'=>'required',
        ];
    }

    public function messages()
    {
        $message = [
                    'first_name.required'=>'وارد کردن نام الزامی است.',
                    'first_name.string'=>'فرمت ورودی صحیح نیست.',

                    'last_name.required'=>'وارد کردن نام خانوادگی الزامی است.',
                    'last_name.string'=>'فرمت ورودی صحیح نیست.',

                    'user_name.required'=>'وارد کردن نام کاربری الزامی است.',
                    'user_name.unique'=>'کاربر با این نام کاربری موجود است.',
                    'user_name.alpha_dash'=>'فرمت ورودی صحیح نیست.',

                    'password.required'=>'انتخاب رمز ورود الزامی است.',
                    'password.alpha_dash'=>'فرمت ورودی صحیح نیست.',

                    'email.required'=>'وارد کردن ایمیل الزامی است.',
                    'email.unique'=>'کاربر با این آدرس ایمیل موجود است.',
                    'email.email'=>'فرمت ایمیل وارد شده صحیح نیست.',

                    'phone_number.required'=>'وارد کردن شماره همراه الزامی است.',
                    'phone_number.unique'=>'کاربر با این شماره همراه موجود است.',
                    'phone_number.regex'=>'فرمت شماره همراه وارد شده صحیح نیست.',

                    'status.required'=>'انتخاب وضعیت کاربر ابزامی است.',

                    'level.required'=>'انتخاب سطح دسترسی کاربر الزامی است.',
        ];

        return array_merge(parent::messages(), $message);
    }
}
