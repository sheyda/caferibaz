<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\NewCategoryRequest;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function index()
    {
        $category = Category::query()->orderByDesc('id')->paginate(5);;
        return view('dashboard.categories.category', compact('category'));
    }

    public function create()
    {
        $category = new Category();
        return view('dashboard.categories.category-new', compact('category'));

    }

    public function show(Category $category_id)
    {
        return view('dashboard.categories.category-edit', compact('category_id'));
    }

    public function store(NewCategoryRequest $request)
    {
        $category = new Category();
        $category->title = $request->title;
        $this->storeImage($category, $request);
        $category->save();
        return redirect()->route('category.show');
    }

    public function update(NewCategoryRequest $request, Category $category_id)
    {
        $category_id->title = $request->title;
        $this->storeImage($category_id, $request);
        $category_id->save();
        return redirect()->route('category.show');

    }

    public function delete(Categ $category_id)
    {
        $category_id->deleteory();
        return view('dashboard.categories.category');
    }

    private function storeImage($category, $request)
    {
        if ($request->has('icon')) {
            $category->icon = $request->icon->store('uploads', 'public');
        }
    }

}
