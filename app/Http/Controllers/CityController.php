<?php

namespace App\Http\Controllers;

use App\Category;
use App\City;
use App\State;
use Illuminate\Http\Request;
use Symfony\Component\Console\Input\Input;

class CityController extends Controller
{
    public function index()
    {
        $cities = City::query()->where('state_id',request()->input('state_id', 0))
            ->pluck('name', 'id');
        return response()->json($cities);
    }
}
