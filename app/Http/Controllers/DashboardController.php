<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $productGroup = Product::query()->limit(4)->orderByDesc('id')->get();
        return view('dashboard.dashboard',compact('productGroup'));
    }

}
