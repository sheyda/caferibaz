<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\NewProductRequest;
use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function index(Request $request)
    {
        $category = Category::all();
        $category_products=Category::query()->whereHas('products')->get();
        $product_query=Product::query()->orderByDesc('id');



        if ($request->has('category_id')){
            // $filters['category_id']=$request->get('category_id');
            $product_query->where('category_id',$request->get('category_id')) ;
        }
        $product_query=$product_query->paginate(5);
        $products = $product_query;


        return view('dashboard.products.products', compact('products', 'category','category_products'));
    }

    public function show(Product $product_id)
    {
        $category = Category::all();
        return view('dashboard.products.product-edit', compact('product_id', 'category'));
    }

    public function create()
    {
        $category = Category::all();
        $product = new Product();
        return view('dashboard.products.product-new', compact('product', 'category'));
    }

    public function store(NewProductRequest $request)
    {
        $product = new Product();
        $product->category_id = $request->category_id;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->discount = $request->discount;
        $product->status = $request->status;
        $this->storeImage($product, $request);
        $product->save();
        return redirect()->route('products.show');

    }

    public function update(NewProductRequest $request, Product $product)
    {
        $product->category_id = $request->category_id;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->quantity = $request->quantity;
        $product->price = $request->price;
        $product->discount = $request->discount;
        $product->status = $request->status;
        $this->storeImage($product, $request);
        $product->save();
        return redirect()->route('products.show');
    }

    public function delete(Product $product_id)
    {
        $product_id->delete();
        return redirect()->route('products.show');
    }

    private function storeImage($product, $request)
    {
        if ($request->has('icon')) {
            $product->icon = $request->icon->store('uploads', 'public');
        }
    }


    public function addToCart($id , Request $request)

    {
        $quantity=1;
        if($request -> has('quantity') && $request->get('quantity')!=null){
        $quantity = $request->get('quantity');
        }

        $product = Product::find($id);
        if (!$product) {
            abort(404);
        }

        $cart = session()->get('cart');

        // if cart is empty then this the first product1
        if (!$cart ) {
            $cart = [
                $id => [
                    "id"=>$product->id,
                    "title" => $product->title,
                    "quantity" => $quantity,
                    "price" => $product->price,
                    "icon" => $product->icon,
                    "total"=>$product->price*$quantity,


                ]
            ];
            session()->put('cart', $cart);
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }


        // if cart not empty then check if this product1 exist then increment quantity
        if (isset($cart[$id])) {

            $cart[$id]['quantity']+=$quantity;

            session()->put('cart', $cart);

            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }



        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "id"=>$id,
            "title" => $product->title,
            "quantity" => $quantity,
            "price" => $product->price,
            "icon" => $product->icon,
            "total"=>$product->price*$quantity


        ];

        session()->put('cart', $cart);

        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

}
