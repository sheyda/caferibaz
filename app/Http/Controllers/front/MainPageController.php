<?php


namespace App\Http\Controllers\front;


use App\Category;
use App\Http\Controllers\Controller;
use App\Product;

class MainPageController extends Controller
{
    public function index()
    {

        $category = Category::query()->orderByDesc('id')->limit(6)->get();
        $productGroup1 = Product::query()->where('category_id','1')->limit(4)->orderByDesc('id')->get();
        $productGroup2 = Product::query()->where('category_id','2')->limit(4)->orderByDesc('id')->get();
        return view('frontend.front_home', compact('category','productGroup1','productGroup2'));
    }

}
