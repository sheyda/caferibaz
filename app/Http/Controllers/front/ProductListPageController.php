<?php


namespace App\Http\Controllers\front;


use App\Category;
use App\Product;

class ProductListPageController
{
    public function index(Category $category_id)
    {
        $productGroup = $category_id->products()->paginate(8);
        return view('frontend.front_product_list',compact('category_id','productGroup'));
    }

}
