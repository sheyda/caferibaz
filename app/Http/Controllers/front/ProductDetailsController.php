<?php


namespace App\Http\Controllers\front;


use App\Http\Controllers\Controller;
use App\Product;

class ProductDetailsController extends Controller
{
    public function index(Product $product ){
        $productGroup2 = Product::query()->where('category_id',$product->category_id)->limit(4)->orderByDesc('id')->get();
        foreach ($productGroup2 as $key => $product2) {
            if($product2->id == $product->id) {
                unset($productGroup2[$key]);
            }
        }
        return view('frontend.ProductDetails' , compact('product' , 'productGroup2'));
    }
}
